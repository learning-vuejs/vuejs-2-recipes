import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import routes from '@/routes';
import 'bootstrap/dist/css/bootstrap.css';
import Axios from 'axios';

Axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com';
Axios.defaults.headers.common.Accept = 'application/json';

Vue.$http = Axios;
Object.defineProperty(Vue.prototype, '$http', {
  get() {
    return Axios;
  }
});

Vue.use(VueRouter);

export const router = new VueRouter({
  routes,
  linkActiveClass: 'active'
});

import state from './state';

if (localStorage.getItem('isLoggedIn') === '1') {
  state.isLoggedIn = true;
}

/* eslint-disable no-console */
router.beforeEach((to, from, next) => {
  console.log('<before-each>');
  console.log('To: ', to);
  console.log('From: ', from);
  console.log('</before-each>');
  if (to.matched.some(record => record.meta.auth) && !state.isLoggedIn) {
    alert('Sorry, you are not allowed to view this page...');

    next({
      name: 'login',
    });
  } else if (to.matched.some(record => record.meta.guest) && state.isLoggedIn) {
    alert('Sorry, you cannot be logged in to view this page...');

    next({
      name: 'home',
    });
  } else {
    next();
  }
});

// Vue Router afterEach navigation guard
router.afterEach((to, from) => {
  console.log('<after-each>');
  console.log('To: ', to);
  console.log('From: ', from);
  console.log('</after-each>');
});

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');

import {ADD, REMOVE, TOGGLE_EDIT, UPDATE,} from './mutation-types';

export default {
  [ADD](state, todo) {
    const lastTodo = state.all.slice(-1).pop();

    state.all.push({
      title: todo,
      completed: false,
      id: lastTodo ? lastTodo.id + 1 : 1,
      isEdited: false,
    });
  },
  [REMOVE](state, todo) {
    state.all = state.all.filter(item => item !== todo);
  },
  [TOGGLE_EDIT](state, todo) {
    todo.isEdited = !todo.isEdited;
  },
  [UPDATE](state, { todo, title }) {
    todo.title = title;
  }
}

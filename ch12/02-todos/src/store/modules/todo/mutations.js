import {
  FETCH,
  CLEAR
} from './mutation-types';

export default {
  [FETCH](state, items) {
    state.all = items;
  },
  [CLEAR](state) {
    state.all = [];
  },
}

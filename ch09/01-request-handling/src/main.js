import Vue from 'vue';
import App from './App.vue';
import 'bootstrap/dist/css/bootstrap.css';
import Axios from 'axios';

Vue.config.productionTip = false;
// Set the base URL
Axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com';
Axios.defaults.headers.common.Accept = 'application/json';
// Bind Axios to Vue
Vue.$http = Axios;
Object.defineProperty(Vue.prototype, '$http', {
  get() {
    return Axios;
  }
});
/* eslint-disable no-console */
// Add request interceptors
Axios.interceptors.request.use((config) => {
  console.log('Request:', config);

  return config;
}, (error) => {
  alert('Whoops! Something went wrong');
  console.log(error);

  return Promise.reject(error);
});
// Add response interceptors
Axios.interceptors.response.use((response) => {
  console.log('Response:', response);

  return response;
}, (error) => {
  alert('Whoops! Something went wrong');
  console.log(error);

  return Promise.reject(error);
});

new Vue({
  render: h => h(App)
}).$mount('#app');

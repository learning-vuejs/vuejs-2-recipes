import Vue from 'vue';
import App from './App.vue';
import 'bootstrap/dist/css/bootstrap.css';
import axios from 'axios';

axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com/';
axios.defaults.headers.common.Accept = 'application/json';
// Bind Axios to Vue
Vue.$http = axios;
Object.defineProperty(Vue.prototype, '$http', {
  get() {
    return axios;
  }
});

Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount('#app');
